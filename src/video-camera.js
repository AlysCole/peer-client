import { LitElement, html, css } from 'lit';

class VideoCamera extends LitElement {
  static properties = {
    header: { type: String },
    screenshot: {
      type: String,
      state: true
    },
    videoElement: {
      type: Object,
    },
    stream: {
      type: Object,
      state: true,
    },
    waitforinput: {
      type: Boolean,
    }
  }

  static styles = css`
    #videoId{
      width:100%;
    }
    #videoId[is-pip]{
        transform-origin: top left;
        transform: scale(0.2,0.2);
        position: absolute;
        transition: transform 0.3s;
    }
    #videoId[is-closed]{
        display: none;
    }
    button[is=paper-icon-button-light] {
        position: absolute;
        top: 10px;
        left: 10px;
        width: 40px;
        height: 40px;
        border:none;
        background-color:transparent;
    }
  `;

  initializeVideo = async () => {
    // Grab elements, create settings, etc.
    const video = this.renderRoot.querySelector("video#videoId");

    // const context = canvas.getContext("2d");
    // const videoObj = { "video": true,audio: true };
    // const errBack = function(error) {
    //     console.log("Video capture error: ", error.code); 
    // };

    video.srcObject = this.stream;
    video.play();
  }

  // play = () => {
  //   this.videoElement.play();
  // }

  hide = () => {
    const video = this.renderRoot.querySelector("video#videoId");
    video.setAttribute("is-closed", true);
  }

  connectedCallback() {
    super.connectedCallback();

    if (this.stream) this.initializeVideo();
  }

  updated(changedProperties) {
    if (changedProperties.has("stream") && this.stream) {
      this.initializeVideo();
    }
    
    if (changedProperties.has("stream") && !this.stream) {
      this.hide();
    }
  }

  render() {
    return html`
      <div>
        <video id="videoId"></video>
      </div>
    `;
  }
}

customElements.define('video-camera', VideoCamera);