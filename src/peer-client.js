import { LitElement, html, css } from 'lit';

class PeerClient extends LitElement {
  static properties = {
    header: { type: String },
    userStream: {
      type: Object,
      state: true,
    },
    remoteStream: {
      type: Object,
      state: true,
    },
    peerId: {
      type: String,
      state: true,
    }
  }

  static styles = css`
    :host {
      min-height: 100vh;
      display: flex;
      flex-direction: column;
      align-items: center;
      justify-content: flex-start;
      font-size: calc(10px + 2vmin);
      color: #1a2b42;
      max-width: 960px;
      margin: 0 auto;
      text-align: center;
      background-color: var(--peer-client-background-color);
    }

    main {
      flex-grow: 1;
    }

    .logo {
      margin-top: 36px;
      animation: app-logo-spin infinite 20s linear;
    }

    @keyframes app-logo-spin {
      from {
        transform: rotate(0deg);
      }
      to {
        transform: rotate(360deg);
      }
    }

    .app-footer {
      font-size: calc(12px + 0.5vmin);
      align-items: center;
    }

    .app-footer a {
      margin-left: 5px;
    }
  `;

  constructor() {
    super();
    this.header = 'Video Call';
    // eslint-disable-next-line no-undef
    this.peer = new Peer({
      host: "192.168.1.3",
      port: 8050,
      path: "/myapp"
    });

  }

  startCall = async event => {
    event.preventDefault();

    const form = this.renderRoot.querySelector("form");

    const data = new FormData(form);
    try {
      const id = data.get("peerId");
      console.log("Calling id:", id);
    
      if (!this.userStream) await this.getUserMedia();

      this.call = this.peer.call(id, this.userStream);

      this.call.on('stream', stream => {
      // `stream` is the MediaStream of the remote peer.
      // Here you'd add it to an HTML video/canvas element.
        this.remoteStream = stream;
        console.log("Remote stream added:", this.remoteStream);
      });

      this.call.on('close', () => {
        // `stream` is the MediaStream of the remote peer.
        // Here you'd add it to an HTML video/canvas element.
        this.remoteStream = undefined;
        this.userStream = undefined;
      });
    } catch (error) {
      console.error("Error while calling:", error);
    }
  }

  getUserMedia = async () => {
    this.userStream = await navigator.mediaDevices.getUserMedia({
      audio: true,
      video: true
    });

    return this.userStream;
  }

  endCall = () => {
    this.call.close();
  }

  connectedCallback() {
    super.connectedCallback();

    this.peer.on('open', (id) => {
      console.log(`My peer ID is: ${id}`);
      this.peerId = id;
    });

    this.peer.on('call', async call => {
      this.call = call;
      if (!this.userStream) await this.getUserMedia();

      console.log("Answer call with:", this.userStream);
      // Answer the call, providing our mediaStream
      this.call.answer(this.userStream);

      this.call.on('stream', stream => {
        // `stream` is the MediaStream of the remote peer.
        // Here you'd add it to an HTML video/canvas element.
        this.remoteStream = stream;
        console.log("Remote stream added:", this.remoteStream);
      });

      this.call.on('close', () => {
        // `stream` is the MediaStream of the remote peer.
        // Here you'd add it to an HTML video/canvas element.
        this.remoteStream = undefined;
        this.userStream = undefined;
      });
    });
  }

  render() {
    console.log("user stream:", this.userStream);
    console.log("Remote stream:", this.remoteStream);
    return html`
      <main>
        <h1>${this.header}</h1>
        <p>Peer ID: ${this.peerId}</p>
        <form @submit="${this.startCall}">
          <label for="peer-id">Connect To:</label>
          <input name="peerId" />
          <button type="submit">Connect</button>
        </form>
        <button @click="${this.endCall}">Hang Up</button>
        <video-camera id="mywebcam" .stream=${this.userStream}></video-camera>
        <video-camera id="peerstream" waitforinput="true" .stream="${this.remoteStream}"></video-camera>
      </main>
    `;
  }
}

customElements.define('peer-client', PeerClient);